Lightbar for Linux is a desktop panel for Linux. It started as a simple launcher but after some development it has become a fully featured and functional panel.

You can create virtually any sort of applet/plugin and attach it to the panel. 

You do this by creating a main application widget in Qt for you applet and passing its address to the panel. 

To establish a proper integration with the panel, you just need to implement the **panel_applet_interface** class in your main window. 

Lightbar has full Drag&Drop support and it’s fully themeable. 

The panel looks and works good, and I have paid special attention to aestetics and neatness and I have developed an awesome application launcher (generally known as a strart menu) plugin for it. 

Screenshots and code are coming - I am just doing some changes before I make the code available.

Here is a list of plugins which I have personally developed for the panel:


- **Application Menu** (super awesome start menu with full Drag&Drop support + add any file via Drag&Drop)
- **Block Devices Menu** (a button which brings up a menu that shows block devices in the menu and allows mounting, unmounting, launching)
- **Command Runner** (a combo box that sits on the panel and allows you to type commands and launch them, really cool, it has history).
- **Virtual Desktops** buttons
- **Directory Browser** (a button which brings up a menu showing directories and allows you to view and launch directories as well as open a terminal)
- **Drawer** (add any file via Drag&Drop)
- **Launcher**
- **Output Monitor** (display the output of any command)
- **Places** (a button which brings up a menu that shows your GTK bookmarks)
- **Screen Resolution** (a button which brings up a menu that allows you to select a different Display Resolution)
- **Separator**
- **Spacer**
- **Systray Area**
- **Time and Date**
- **Window List** (a taskbar)

That is the base, that gives you a full functionality. Some applets such as the **Command Runner** and **Output Monitor** are resizable. There is a little drag handle at the end which allows you to adjust applet's width.

The project exists, it's functional but there are a few things that need paying attention. This should not take too long (depending on my free time).

The memory consumption is extremely low, hence for the name "light". I have all applets on the panel and it only takes about 0.5MB.

